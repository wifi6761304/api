<?php
namespace API;

class Customer {
	private \PDO $pdo;

	public function __construct(\PDO $pdo) {
		$this->pdo = $pdo;
	}

	/**
	 * Get all customers
	 *
	 * @return array
	 */
	public function getCustomers(): array {
		$stmt = $this->pdo->prepare("SELECT * FROM customers");
		$stmt->execute();
		$customers = $stmt->fetchAll(\PDO::FETCH_ASSOC);
		return $customers;
	}

	/**
	 * Get a single customer by id
	 *
	 * @param integer $id
	 * @return array
	 */
	public function getCustomer(int $id): array {
		$stmt = $this->pdo->prepare("SELECT * FROM customers WHERE customer_id = :id");
		$stmt->execute(["id" => $id]);
		$customer = $stmt->fetch(\PDO::FETCH_ASSOC);
		/*
			ternary operator:
			if else in one line
			condition ? true : false
			condition ?: false

			Kurzform von:
			if ($customer) {
				return $customer;
			} else {
				return [];
			}
		*/
		return $customer ?: [];
	}
}