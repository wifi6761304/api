<?php

namespace API;

class Router
{
	private array $routes;
	public function __construct(array $conf)
	{
		$this->routes = $conf;
	}

	public function resolve(string $urlKey): ?string
	{
		// Query String entfernen, falls vorhanden
		$tempUrl = explode('?', $urlKey)[0];
		$uri = $this->routes[$tempUrl] ?? null;
		return $uri;
	}
}
