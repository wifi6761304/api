<?php
require_once "vendor/autoload.php";
require_once "init.php";

$routesConfig = parse_ini_file(__DIR__ . '/routes.ini', true);

$router = new API\Router($routesConfig);
$reqUri = $_SERVER["REQUEST_URI"];
$route = $router->resolve($reqUri);

if ($route) {
	require_once $route;
} else {
	require "pages/404.php";
}