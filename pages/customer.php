<?php
// Get Parameter cid muss vorhanden sein. Wir lesen Customer getCustomer($id) aus.
require_once __DIR__ . "/../init.php";

$cid = filter_var($_GET["cid"] ?? null, FILTER_VALIDATE_INT);
// cid ist nicht vorhanden. Wir brechen ab.
if (!$cid) {
	exit;
}

$customer = new \API\Customer($pdo);
$singleCustomer = $customer->getCustomer($cid);

if(!$singleCustomer) {
	exit;
}

/*
	PHP kann http header setzen. In unterschiedlichen Situationen sind
	diese Header notwendig. In diesem Fall setzen wir den Content-Type
	auf JSON, damit der Browser weiß, dass er JSON-Daten erhält.

	Http Header müssen vor dem eigentlichen Inhalt gesetzt werden. Dh. es darf
	davor KEINE Ausgabe stattfinden.
 */
// set json header
header("Content-Type: application/json");
echo json_encode($singleCustomer, JSON_PRETTY_PRINT);