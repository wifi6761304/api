<?php
require_once __DIR__ . "/../init.php";

$customer = new \API\Customer($pdo);
$customers = $customer->getCustomers();

/*
	PHP kann http header setzen. In unterschiedlichen Situationen sind
	diese Header notwendig. In diesem Fall setzen wir den Content-Type
	auf JSON, damit der Browser weiß, dass er JSON-Daten erhält.

	Http Header müssen vor dem eigentlichen Inhalt gesetzt werden. Dh. es darf
	davor KEINE Ausgabe stattfinden.
 */
// set json header
header("Content-Type: application/json");
echo json_encode($customers, JSON_PRETTY_PRINT);